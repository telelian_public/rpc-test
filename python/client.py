from time import sleep, time
from loguru import logger
import msgpackrpc

cli = msgpackrpc.Client(msgpackrpc.Address("localhost", 9001))

while True:
    cnt = cli.call('delaycnt')
    logger.debug(f'cnt: {cnt}')
    msg = cli.echo('cli msg')
    logger.debug(f'msg from server : {msg} ')
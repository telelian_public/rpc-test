from time import sleep, time
from loguru import logger
import msgpackrpc


class TestServer(object):
    def __init__(self):
        self.cnt = 0
    def echo(self, msg):
        #logger.debug(f'echo {msg} class')
        return f'{msg} recved class'
    
    def getDict(self, msg):
        return {
            'cnt': f'{self.cnt}',
            'msg': msg
        }
    
    def getList(self, msg):
        return msg.split()

    def delaycnt(self):
        sleep(0.5)
        self.cnt += 1
        return self.cnt

    def noreturn(self):
        logger.debug('noret')

serv = TestServer()

# start = time()
# serv.echo('test')
# end = time()
# logger.debug(f'elapsed : {(end-start)*1000}ms')

server = msgpackrpc.Server(serv)
server.listen(msgpackrpc.Address("localhost", 9001))
server.start()
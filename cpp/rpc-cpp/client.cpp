#include "rpc/client.h"
#include <iostream>
#include <chrono>
#include <stdio.h>
#include <string>
#include <vector>
#include <map>
#include "json.hpp"
using json = nlohmann::json;

using std::string;
using std::vector;
using std::map;

using namespace std::chrono;

int main() {
    rpc::client c("localhost", 9001);
    c.set_timeout(2000);

    string input, result;
    typedef vector<string> RESVEC;
    RESVEC resVec;
    typedef map<string, string> RESDICT;
    RESDICT resDict;
    
    // while (std::getline(std::cin, input)) {
    //     if (!input.empty()) {
    //         auto start = system_clock::now();
    //         result = c.call("echo", input).as<string>();
    //         auto end = system_clock::now();
    //         duration<double> elapsed = end - start;
    //         std::cout << result <<" elapsed:"<< elapsed.count() <<std::endl;
            
    //         start = system_clock::now();
    //         c.call("noreturn");
    //         end = system_clock::now();
    //         elapsed = end - start;
    //         std::cout << "no ret" <<" elapsed:"<< elapsed.count() <<std::endl;


    //         resDict = c.call("getDict", input).as<RESDICT>();
    //         std::cout << "\ndict" <<std::endl;
    //         for (auto i = resDict.begin(); i != resDict.end(); i++)
    //             std::cout << i->first<<":"<<i->second << std::endl;

    //         std::cout << "\nlist" <<std::endl;
    //         resVec = c.call("getList", input).as<RESVEC>();
    //         for (RESVEC::iterator i= resVec.begin(); i != resVec.end(); i++)
    //             std::cout << *i << std::endl;
            
    //     }
    // }
    while(1)
    {
        auto cnt = c.call("delaycnt").as<int>();
        std::cout<<"cnt"<<cnt<<std::endl;
    }
}
# rpc test

## c++
- rpclib : <http://rpclib.net/>

### requirements
- cmake
- g++ 4.8

### compile
```sh 
git clone https://github.com/rpclib/rpclib.git
cd rpclib
mkdir build
cd build
cmake ..
cmake --build .
```

### link

- rpclib/build/librpc.a를 link 시킬 것.

## python
- zerorpc : <https://github.com/0rpc/zerorpc-python>
